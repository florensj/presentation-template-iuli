
# Table of Contents

1.  [How to use this template](#org0c2b481)



<a id="org0c2b481"></a>

# How to use this template

1.  Fork this repo;
2.  Rename the name<sub>thesis</sub><sub>defense.tex</sub> to your<sub>name</sub><sub>thesis</sub><sub>defense.tex</sub>;
3.  Compile it using XeLaTeX;
4.  If there is error complaining about Fira Fonts, you can either install the fonts to your OS or comment the following lines:
    -   \usefonttheme{professionalfonts} % required for mathspec
    -   \usepackage{mathspec}
    -   \setsansfont[BoldFont={Fira Sans}, Numbers={OldStyle}]{Fira Sans Light}
    -   \setmathsfont(Digits)[Numbers={Lining, Proportional}]{Fira Sans Light}

